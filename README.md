# Heisenbug Workshop: Android UI Tests

## Общий план

Презентация:
- Пару слов о себе
- [Автотесты под Android: Общая картина](https://whimsical.com/android-autotests-JJZE4uUcpSx26b2TeDM9jU)
- Про что будет Workshop

## Представление примера (Heisenbug)

- Презентация (Heisenbug app)
- [Приложение Heisenbug](https://gitlab.com/EvgeniiMatsiuk/heisenbug)
- Все этапы разбиты по веткам
- Открыть приложение в Android Studio и запустить его

## Процесс написания тестов

### Почему Kaspresso

Презентация (Почему Kaspresso, Механизм Интерсепторов)

### Подключение Kaspresso + первый тест

- Branch: ***first_step***
- Интеграция по [Readme](https://github.com/KasperskyLab/Kaspresso#integration)
    - просмотр `:app:build.gradle`
    - `mavenCentral` подключен в `settings.gradle`
- Добавляем разрешения на чтение и запись с памяти
    - `:app:AndroidManifest.xml` просмотр
    - Storage issues расписаны в [README](https://github.com/KasperskyLab/Kaspresso#storage-issues)
- Описываем первые экраны
    - Просмотр `FirstScreen`
        - Что такое `layoutId` и `viewClass`
        - Описываем `helloText` и `nextButton`
    - Просмотр `SecondScreen`
- Описываем первый тест `SimpleTest`
    - Что такое `TestCase` и зачем от него наследоваться
    - Что такое `runtimePermissionRule`
    - Что такое `activityRule`
    - Описываем метод `test`
- Запуска теста
    - Просмотр логов

### Добавление экранов для flaky tests и списков

- Branch: ***second_step***
- Просмотр обновленного приложения

### Обработка flaky tests

- Branch: ***third_step***
- Просмотр `FlakyTest`
- Запуск теста

#### Касмотизация обработки flaky-тестов

Первый способ - использование `flakySafely`
```kotlin
// 1
FlakyFragment.TIMEOUT_MS = 12_000L

// 2
class FlakyTest : TestCase() {

    //...

    @Test
    fun test() = run {
        // ...

        step("Count and check") {
            FlakyScreen {
                countButton {
                    click()
                }

                counterTextView {
                    flakySafely(timeoutMs = 15_000) { // <===
                        hasText("Counter: 1")
                    }
                }

                countButton {
                    click()
                }

                counterTextView {
                    flakySafely(timeoutMs = 15_000) { // <===
                        hasText("Counter: 1")
                    }
                }
            }
        }
    }
}
```

Второй способ - настройка конфигурации
```kotlin
// 1
FlakyFragment.TIMEOUT_MS = 12_000L

// 2
class FlakyTest : TestCase(
    kaspressoBuilder = Kaspresso.Builder.simple {
        flakySafetyParams = FlakySafetyParams.custom(timeoutMs = 15_000)
    }
) {
    // ...
}
```

#### Улучшение кода с Scenario

- Branch: ***third_step_2***
- Просмотр `BeginningScenario`
- Просмотр `FlakyTest`, `SimpleTest`
- Запуск тестов и просмотр логов
    - Обратить внимание на `TEST STEP: "1.1. Open the First Screen and Check" in SimpleTest`

### Работа со списками (Опционально)

- Branch: ***fourth_step***
- Просмотр `ListScreen`
    - Проблема: у элементов списка нет уникальных id в вертске
- Просмотр `ListTest`

### Kaspresso samples

- Открываем проект Kaspresso
- Начинаем с `samples/kaspresso-sample/src/androidTest`
    - Посмотреть `autoscrollfallback`, `compose`, `device`, `UiCommonFlakyTest` in `flaky_tests`
- Если есть время, то просмотреть и другие примеры

### Что почитать/посмотреть по Kaspresso

- [README](https://github.com/KasperskyLab/Kaspresso)
- [RU] [Дмитрий Мовчан, Евгений Мацюк — Как начать писать автотесты и не сойти с ума](https://youtu.be/q_8UUhVDV7c)
- [RU] [Егор Курников — Единственное, что вам нужно для UI-тестирования](https://youtu.be/cTykctRSmuA)
- [EN] [Eugene Matsyuk — How to start writing autotests and not go crazy](https://www.youtube.com/watch?v=xiVDqMlTdbM&feature=youtu.be)
- [RU] [Евгений Мацюк — Kaspresso: фреймворк для автотестирования, который вы ждали](https://habr.com/ru/company/kaspersky/blog/467617/)
- [EN] [Eugene Matsyuk — Kaspresso: The autotest framework that you have been looking forward to. Part I](https://proandroiddev.com/kaspresso-the-autotest-framework-that-you-have-been-looking-forward-to-part-i-e102ed384d11)
- [RU] [Воркшоп по автотестам. 19-12-2019](https://www.youtube.com/watch?v=FExlaWfKENI)
- [RU] [Live-coding: мобильные автотесты с нуля / Екатерина Батеева, Руслан Мингалиев](https://www.youtube.com/watch?v=gFPeH2yihDA)
- [RU] [Android Advanced #5: Testing Part 2](https://www.youtube.com/watch?v=G12Lgs7FaQc&list=PLjLCGE4bVpHD3DDTvKXknKTnE8I7mu3hF&index=5)
- [RU] [Kaspresso tutorials. Часть 1. Запуск первого теста](https://habr.com/ru/company/kaspersky/blog/570658/)
- [RU] [Adb-server в Kaspresso](https://habr.com/ru/post/594017/)

## Test Runner

### Теория

Презентация (Работа инструментальных тестов, Runners)

### AndroidJUnitRunner

```bash
# когда нажимаете на зеленую кнопку, то стартует команда
./gradlew :app:connectedDebugAndroidTest

# что она делает под капотом
# зайдите в директорию Heisenbug

# сборка apk
./gradlew app:assembleDebug app:assembleDebugAndroidTest

# установка apk
adb install app/build/outputs/apk/debug/app-debug.apk
adb install app/build/outputs/apk/androidTest/debug/app-debug-androidTest.apk

# запуск теста
adb shell am instrument -w -e class com.matsiuk.heisenbug.test.SimpleTest  com.matsiuk.heisenbug.test/androidx.test.runner.AndroidJUnitRunner
# targetApk = com.matsiuk.heisenbug
# testApk = com.matsiuk.heisenbug.test

# доступные опции по запуску тестов - https://developer.android.com/studio/test/command-line

# теперь попробуем запустить все тесты
adb shell am instrument -w com.matsiuk.heisenbug.test/androidx.test.runner.AndroidJUnitRunner

# тесты могут флэкать по причине анимации
# один из способов отключения анимации
adb shell "settings put global window_animation_scale 0.0"
adb shell "settings put global transition_animation_scale 0.0"
adb shell "settings put global animator_duration_scale 0.0"
adb shell "settings put secure spell_checker_enabled 0"
adb shell "settings put secure show_ime_with_hard_keyboard 1"
```

#### Масштабирование

```bash
# предварительно запускаем два эмулятора
adb -s emulator-5554 shell am instrument -e numShards 2 -e shardIndex 0  -w com.matsiuk.heisenbug.test/androidx.test.runner.AndroidJUnitRunner
adb -s emulator-5556 shell am instrument -e numShards 2 -e shardIndex 1  -w com.matsiuk.heisenbug.test/androidx.test.runner.AndroidJUnitRunner
```

#### Проблема шареного состояния

Branch: ***fifth_step*** </br>
Смотрим, как шареное состояние приводит к флекам на примере `SharedState1`, `SharedState2`:
```bash
# не забываем собрать и установить app-debug.apk и app-debug-androidTest.apk
# затем запускаем тесты
adb shell am instrument -w -e package com.matsiuk.heisenbug.test.shared  com.matsiuk.heisenbug.test/androidx.test.runner.AndroidJUnitRunner
```

### Orchestrator

- Презентация (Orchestrator)
- [Документация](https://developer.android.com/training/testing/instrumented-tests/androidx-test-libraries/runner#use-android)
- Branch: ***sixth_step***
  - Смотрим `app/build.gradle`
  - Посмотреть репорты в `app/build/reports/androidTest/connected/index.html`
  - Посмотреть репорты, когда один тест падает

Запуск в консоли:
```bash
adb shell 'CLASSPATH=$(pm path androidx.test.services) app_process / \
 androidx.test.services.shellexecutor.ShellMain am instrument -w -e \
 targetInstrumentation com.matsiuk.heisenbug.test/androidx.test.runner.AndroidJUnitRunner \
 androidx.test.orchestrator/.AndroidTestOrchestrator'
```

### Marathon

- Презентация (Marathon)
- [Документация](https://marathonlabs.github.io/marathon/)
- Branch: ***seventh_step***
  - Смотрим `config/Marathonfile`
- Посмотреть Allure репорты, когда один тест падает

#### Как работать

Установка:
```bash
brew tap malinskiy/tap
brew install malinskiy/tap/marathon
```

Запуск:
```bash
# сборка apk
./gradlew app:assembleDebug app:assembleDebugAndroidTest

# старт марафона
marathon -m config/Marathonfile

# просмотр Allure отчетов
# установка
brew install allure
# формирование отчета
allure serve build/reports/marathon/allure-results
```

### Что почитать/посмотреть

- [RU] [Android Advanced #6: Testing cases in HH.ru, AliExpress and Revolut](https://www.youtube.com/watch?v=6XW6T0QOPpc&t=3437s)

## На чем запускать тесты

Презентация (На чем запускать тесты)

### Пример запуска тестов в Docker container

Рассмотрим пример старта реального докер образа. 
`registry.gitlab.com/evgeniimatsiuk/heisenbug/emulator-full-29:2022.10.26.5` сделан на основе решений от [Avito](https://github.com/avito-tech/avito-android/tree/develop/ci/docker) и [Agoda](https://github.com/agoda-com/docker-emulator-android). 
Внутри образа помещены Android Emulator + AdbServer + Marathon.

Пример старта:
```bash
# Real docker image run example
docker run \
    -d \
    -e "SNAPSHOT_DISABLED"="true" \
    -e "WINDOW"="true" \
    -e "GPU_ENABLED"="false" \
    -e "PROXY"="your_proxy" \
    -e "START_SCRIPT"="your_start_script.sh"
    -e "FINAL_SCRIPT"="your_final_script.sh"
    -e "MARATHON_SERIAL"="111" \
    -e "TIMEZONE"="Europe/Moscow" \
    -e "ANDROID_CONFIG"="skin.name=600x1024;hw.lcd.density=160;hw.lcd.height=600;hw.lcd.width=1024;" \
    -e "ADB_SERVER_ON"="true" \
    -e "MARATHON_FILE"="MARATHONFILE" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --volume="/path/to/artifacts:/work" \
    -p 5557:5555 \
    -p 5556:5554 \
    --privileged \
    --name emulator \
    --rm \
    registry.gitlab.com/evgeniimatsiuk/heisenbug/emulator-full-29:2022.10.26.5
```

Explanation:
1. `docker run` is a command to run your image `registry.gitlab.com/evgeniimatsiuk/heisenbug/emulator-full-29:2022.10.26.5` that is written on the last row. 
The full documentation about `docker run` is available [here](https://docs.docker.com/engine/reference/run/#docker-run-reference).
2. `-d` means your container works in the background (detached mode) and doesn't output logs on your screen until you want to see logs explicitly. 
More information about foreground and detached modes is available [here](https://docs.docker.com/engine/reference/run/#detached-vs-foreground).
3. `-e` gives a possibility to describe environment variables using in Dockerfile and shell scripts that are started by Dockerfile.
   1. `SNAPSHOT_DISABLED` determines an emulator uses snapshot or not.
   2. `WINDOW`. If you want to see UI of emulator then set this flag in true.
   3. `GPU_ENABLED` parameter is in charge of using whether GPU or software rendering to render emulators. 
   Here, a developer has preferred GPU to render. But, our team prefers software rendering especially in cases when an emulator is started in a headless mode. 
   That's why we are going to miss this flag in the following examples, but keep in mind the possibility to use gpu for rendering your emulator.
   4. `PROXY` is just a proxy for your emulator.
   5. `START_SCRIPT` sets a name of a script that will be executed before a test running. Optional parameter.
   6. `FINAL_SCRIPT` sets a name of a script that will be executed at the end regardless of the result (success or fail of tests). Optional parameter.
   7. `TIMEZONE`. A timezone in your emulator.
   8. `ADB_SERVER_ON` allows a developer to turn on/off AdbServer. By default, AdbServer is turned off.
   9. `ADB_SERVER_NEW` is a temporary flag. Allows a developer to turn on AdbServer-desktop 1.2.0. By default, this flag is false (old AdbServer (1.1.0) is turned on).
   10. `ANDROID_CONFIG` gives you an opportunity to override initial config.ini for your emulator.
   11. `MARATHON_FILE` is a variable describing the name of special file using by Marathon runner to start tests. 
   Please, keep in mind that the mentioned special file must be among artifacts bounded through 
4. `--volume="/path/to/artifacts:/work"` or must be bounded through another mount.
5. `--volume="/tmp/.X11-unix:/tmp/.X11-unix:rw"`. The key word `volume` helps to bind mounts. 
Explaining info is available by [this reference](https://docs.docker.com/storage/volumes/). 
Here, we give an access to [Xorg](https://wiki.archlinux.org/index.php/Xorg) from Linux host to the container. 
Such access makes possible the container draws an emulator and you can see the emulator.
6. `-p` binds your container's port with localhost's port. 
For example, `-p 5556:5554` means that your container's port "5554" is listening actions from localhost's port "5556". It's important to notice container's port "5554" must be exposed in Dockerfile (the key word `EXPOSE`). 
Thanks to such port forwarding, when container is running, we can write `adb connect localhost:5556` in localhost and connect to our emulator that is inside the container.
7. `--priveleged`. Just use this flag and don't think about it now. The flag turns on a special priveleged mode for container that is necessary to start an emulator inside docker container.
8. `--name` sets name of a container. So, it's more convenient to use name as a references to an image than image's id like "856210b6c8b0".
9. `--rm` is responsible for auto removing of stopped container.

Обзор Dockerfile и вспомогательных скриптов:
- `Dockerfiles/android_minimal/Dockerfile`
- `emulator-base/Dockerfile`
- `emulator-base/prepare_snapshot_stage_1.sh`
- `emulator-base/check_readiness.sh`
- `marathon-image/entrypoint-emulator-hive.sh`

Что почитать про Docker:
- [Обучающие статьи на Habr про Docker](https://habr.com/ru/company/ruvds/blog/438796/)
- [Обучающие статьи на Habr про Docker-Compose](https://habr.com/ru/company/ruvds/blog/450312/)
- Документация про [Docker](https://docs.docker.com/) и [Docker-Compose](https://docs.docker.com/compose/)

## Инфраструктура

### Краткое инфо

Презентация (Инфраструктура)

### GitLab CI

- [Готовый репозиторий в GitLab](https://gitlab.com/EvgeniiMatsiuk/heisenbug)
- [Готовый пайплайн GitLab CI/CD](https://gitlab.com/EvgeniiMatsiuk/heisenbug/-/pipelines)
- Секреты: Settings -> CI/CD -> Variables
- Рассматриваем `gitlab-ci.yml` в "Editor".

### Firebase Test Lab

Создаем аккаунт в [Google Cloud](https://console.cloud.google.com):

[<img src="images/ftl_image_1.png" width="700"/>](images/ftl_image_1.png)

Включаем "Cloud Testing API" (вводим в поиске):

[<img src="images/ftl_image_2.png" width="700"/>](images/ftl_image_2.png)<br>
[<img src="images/ftl_image_3.png" width="700"/>](images/ftl_image_3.png)

Таким же образом включаем "Cloud Tool Results API", "Cloud Functions API".

Следующим действием необходимо создать "Service Account" и загрузить его json:

[<img src="images/ftl_image_4.png" width="300"/>](images/ftl_image_4.png)<br>
[<img src="images/ftl_image_5.png" width="700"/>](images/ftl_image_5.png)<br>
[<img src="images/ftl_image_6.png" width="700"/>](images/ftl_image_6.png)

Загруженный Json необходимо сохранить. Также нам понадобится "Project ID". Узнать его можно так:

[<img src="images/ftl_image_7.png" width="400"/>](images/ftl_image_7.png)<br>
[<img src="images/ftl_image_8.png" width="700"/>](images/ftl_image_8.png)

Json и Project ID сохраняем в секреты в GitLab CI.

Не забываем кстати подключить созданный в [Google Cloud](https://console.cloud.google.com) проект к [Firebase Console](https://console.firebase.google.com):

[<img src="images/ftl_image_9.png" width="700"/>](images/ftl_image_9.png)<br>
[<img src="images/ftl_image_10.png" width="400"/>](images/ftl_image_10.png)

В последней картинке выбираете созданный вами в [Google Cloud](https://console.cloud.google.com) проект.

Ссылки:
- [GitLab CI + Firebase Test Lab, бесплатное облачное тестирование Android приложения / ITКультура](https://www.youtube.com/watch?v=BLvhlPDDLts)
- [gcloud firebase test android run](https://cloud.google.com/sdk/gcloud/reference/firebase/test/android/run)

### TestWise

С TestWise необходимо сделать только три простых действия:
1. Написать [сюда](https://testwise.ru/anketa), чтобы получить промокод. В имени укажите "Heisenbug 2022". Пример - Евгений Мацюк "Heisenbug 2022".
2. Регистрируетесь по присланной ссылке.
3. Логин и пароль сохраняете в секретах GitLab CI. Консоль доступна [здесь](https://app.testwise.pro).

### Сравнение инструментов

Открыть [успешный прогон](https://gitlab.com/EvgeniiMatsiuk/heisenbug/-/pipelines/687685748) и [прогон с одним упавшим тестом](https://gitlab.com/EvgeniiMatsiuk/heisenbug/-/pipelines/687686048).

На что обратить внимание при сравнение Firebase Test Lab и TestWise:
- регистрация и настройка
- время выполнения job
- рантайм
- отчеты
  - [TW скоро поддержит Allure отчеты от тестов](https://camo.githubusercontent.com/30f4901eca91856ae42df54cb7fbdc14065929ea3aa37c84a4967967df8e5845/68747470733a2f2f686162726173746f726167652e6f72672f776562742f74712f74372f63682f7471743763686364637a72676475686f756b7168783165727466632e706e67)
- видео
- логи
- очистка состояния
- ретраи
  - FTL предоставляет через gcloud
  - TW делает ретраи автоматически и на разных эмуляторах
  - Обратить внимание на время прогонов [FTL](https://gitlab.com/EvgeniiMatsiuk/heisenbug/-/jobs/3276009269) и [TW](https://gitlab.com/EvgeniiMatsiuk/heisenbug/-/jobs/3276009268) в случае упавшего теста
- работа с флеки тестами
  - FTL - ничего
  - TW - есть политика для работы с флекающими тестами
- масштабируемость
  - FTL - через врапперы и костыли
  - TW - автоматически с оптимизацией скорости прогона по истории